## Springboot Gatling Tests

Gatling performance load testing integrated onto a Springboot API

### Link to Gitlab Pages Report
https://xomethula.momentum.gitlab.io/springboot-gatling-tests/

### Getting started
***

#### Prerequisites
* Java 11
* Gradle 

Run Gatling Tests

```
gradle clean gatlingRun
```

***
### Comparison between Gatling & JMeter

| ##################          | Gatling | JMetere |
| ---------- | ------- | ------- |
| Ease of writing tests | (Yes) Ability to create completely scripted tests using Scala programming language. | (Yes) Requires use of JMeter Graphical Interface, no scripting capability.
| Possible reusability of tests | (Yes) | (No) |
| Reporting capabilities | (Yes) Both Gatling and JMeter generate a beautiful detailed HTML report that is interactive allowing you to perform more analyses on the test results. | - |
| Feature richness | (Yes) | (Yes) |
| Popularity and support of tool | (Yes) | (Yes) |
| Ease integration with spring boot | (Yes) Gatling doesn’t provide an official plugin for gradle. Uses a third-party [gradle-gatling-plugin](https://github.com/lkishalmi/gradle-gatling-plugin) | (Yes) JMeter also integrate with spring boot by using a third-party [jmeter-gradle-plugin](http://jmeter.foragerr.net/) |
| Ease integration with CI pipeline | (Yes) Run the gradle task 'gradle gatlingRun' in one of the stages on the CI pipeline | - |
| Environment configurable | (No) Configuration are made in the gatling.conf file. | (No) |
| Secret management | - | - |