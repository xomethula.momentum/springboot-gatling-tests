import io.gatling.core.Predef._
import io.gatling.http.Predef._

class ApiGatlingSimulationTest extends Simulation {

  val httpProtocol = http
    .baseUrl("https://investments-tst.herokuapp.com/api")

  val userString = "{\"firstName\":\"Scala\", \"lastName\":\"Java\",\"gender\":\"Male\"}"

  val scn = scenario("AddPersonTest")
    .exec(http("POST request")
      .post("/persons")
      .header("Content-Type", "application/json")
      .body(StringBody(userString))
      .check(status.is(200)))
    .pause(2)
    .exec(http("GET request")
      .get("/persons")
      .header("Content-Type", "application/json")
      .check(status.is(200)))

  setUp(scn.inject(constantUsersPerSec(10) during(10))).protocols(httpProtocol);
}
