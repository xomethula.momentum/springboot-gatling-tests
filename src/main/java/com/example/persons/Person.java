package com.example.persons;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Getter
@Setter
@ToString
@Document(collection = "persons")
public class Person {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String gender;
    private String date = new Date().toString();
    
}
