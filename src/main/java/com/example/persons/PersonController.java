package com.example.persons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class PersonController {

    @Autowired
    private PersonService personService;

    private Logger logger = LoggerFactory.getLogger(Person.class);

    @PostMapping("/persons")
    public Person addPerson(@RequestBody Person person) {
        logger.info("Adding new person info");
        return personService.addPerson(person);
    }

    @GetMapping("/persons")
    public List<Person> getAllPersons() {
        logger.info("Retrieving all persons info");
        return personService.getAllPersons();
    }

}
